class BooksController < ApplicationController
 before_action :librarian_signed_in?, only: [:new, :create, :edit, :update, :destroy]

  def index
    #@books = Book.all
    @books = Book.order("updated_at DESC").paginate(:page => params[:page], :per_page => 10)
  end

  def new
     @book = Book.new


  end

  def create
    @book = Book.new(book_params)
    #@book.image = params[:book]
    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: 'Book was succesfuly created.' }
        format.json { render action: 'index', status: :created, location: @book }
      else
        format.html { render action: 'new' }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @book = Book.find(params[:id])
  end

  def edit
      @book = Book.find(params[:id])
  end

  def update
    @book = Book.find(params[:id])

    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    respond_to do |format|
      format.html { redirect_to(books_url) }
      format.json { head :no_content }
    end
  end

  def book_params
    params.require(:book).permit(:title, :release_date, :author_id, :avatar)
  end
end
