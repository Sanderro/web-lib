class ReceiptsController < ApplicationController
  def index
    @receipts = Receipt.order("updated_at DESC").paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @receipt = current_reader.receipts.find(params[:id])
  end

  def new
  end

  def create
    @receipt = current_reader.receipts.build(receipt_params)
    @receipt.status = Receipt::NEW
    respond_to do |format|
      if @receipt.save
        format.html { redirect_to @receipt, notice: 'Receipt was succesfuly created.' }
        format.json { render action: 'index', status: :created, location: @book }
      else
        format.html { redirect_to books_path, alert: "Nie udało się dodać książki do zamówień" }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit

  end

  def update
    @receipt = Receipt.find params[:id]
    @status = @receipt.status

    if params[:reader_canceled] && @receipt.reader == current_reader && @status == Receipt::NEW
      @receipt.status = Receipt::READER_CANCELED
    end

    if params[:cancel_active] && @receipt.reader == current_reader && (@status == Receipt::ACTIVE || @status == Receipt::READY )
      @receipt.status = Receipt::DELETED_ACTIVE
    end

    if @receipt.librarian == nil
      if params[:accepted] && @status == Receipt::NEW
        @receipt.librarian = current_librarian
        @receipt.status = Receipt::ACTIVE
      end
    end

    if params[:librarian_canceled] && @receipt.librarian == current_librarian && (@status == Receipt::NEW || @status == Receipt::ACTIVE || @status == Receipt::READY)
      @receipt.status = Receipt::LIBRARIAN_CANCELED
    end

    if params[:ready] && @receipt.librarian == current_librarian && @status == Receipt::ACTIVE
      @receipt.status = Receipt::READY
    end

    if params[:shipped] && @receipt.librarian == current_librarian && @status == Receipt::READY
      @receipt.status = Receipt::SHIPPED
    end

    if params[:returned] && @receipt.librarian == current_librarian && @status == Receipt::SHIPPED
      @receipt.status = Receipt::RETURNED
    end
    if @receipt.librarian == current_librarian

    end



    @receipt.save!
    redirect_to receipts_path
  end

  def destroy

  end

  def receipt_params
    params.require(:receipt).permit(:book_id)
  end
end
