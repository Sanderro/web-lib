module ReceiptsHelper

  def status_for(receipt)
    case receipt.status
    when Receipt::NEW
      "New"
    when Receipt::READER_CANCELED
      "Canceled by reader"
    when Receipt::LIBRARIAN_CANCELED
      "Canceled by librarian"
    when Receipt::CANCELED_ACTIVE
      "Pending deletion"
    when Receipt::ACTIVE
      "In processing"
    when Receipt::READY
      "Ready to ship"
    when Receipt::SHIPPED
      "Shipped to reader"
    when Receipt::RETURNED
      "Returned by reader"
    else
       "NIEZNANY STATUS"
    end
  end
end

