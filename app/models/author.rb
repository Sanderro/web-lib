class Author < ActiveRecord::Base
  has_many :authors_books
  has_and_belongs_to_many :books
end
