class Book < ActiveRecord::Base
  has_many :authors_books
  has_and_belongs_to_many :authors

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" },
                    :default_url => "/images/:style/missing.png"
  validates_attachment_presence :avatar
end
