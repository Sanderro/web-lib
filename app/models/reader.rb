class Reader < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :receipts
  has_many :penalties, :through => :receipts
  has_many :books, :through => :receipts
  validates :name, :surname, :birth_date, :email, presence: true
end
