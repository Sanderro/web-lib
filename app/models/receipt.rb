class Receipt < ActiveRecord::Base
  belongs_to :reader
  belongs_to :penalty
  belongs_to :librarian
  belongs_to :book

  validates_presence_of :book
  validates_presence_of :reader

  NEW = 1
  READER_CANCELED = 2
  ACTIVE = 3
  LIBRARIAN_CANCELED = 4
  READY = 5
  SHIPPED = 6
  RETURNED = 7
  CANCELED_ACTIVE = 8

  STATUSES = [NEW, READER_CANCELED, ACTIVE, LIBRARIAN_CANCELED, READY, SHIPPED, RETURNED, CANCELED_ACTIVE]

  validates_inclusion_of :status, :in => STATUSES

end
