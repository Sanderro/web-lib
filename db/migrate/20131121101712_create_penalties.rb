class CreatePenalties < ActiveRecord::Migration
  def change
    create_table :penalties do |t|
      t.date :issue_date
      t.string :penalty_type
      t.integer :penalty_score
      t.boolean :is_settled

      t.timestamps
    end
  end
end
