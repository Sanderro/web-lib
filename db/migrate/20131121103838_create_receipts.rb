class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.datetime :issue_date
      t.references :reader, index: true
      t.references :penalty, index: true
      t.references :librarian, index: true

      t.timestamps
    end
  end
end
