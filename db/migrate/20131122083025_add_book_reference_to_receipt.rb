class AddBookReferenceToReceipt < ActiveRecord::Migration
  def change
    add_reference :receipts, :book, index: true
  end
end
