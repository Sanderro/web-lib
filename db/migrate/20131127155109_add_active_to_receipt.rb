class AddActiveToReceipt < ActiveRecord::Migration
  def change
    add_column :receipts, :active, :boolean
  end
end
