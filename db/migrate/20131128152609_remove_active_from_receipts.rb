class RemoveActiveFromReceipts < ActiveRecord::Migration
  def change
    remove_column :receipts, :active, :boolean
  end
end
