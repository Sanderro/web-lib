# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131128152609) do

  create_table "administrators", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "administrators", ["email"], name: "index_administrators_on_email", unique: true, using: :btree
  add_index "administrators", ["reset_password_token"], name: "index_administrators_on_reset_password_token", unique: true, using: :btree

  create_table "authors", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authors_books", force: true do |t|
    t.integer  "book_id"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authors_books", ["author_id"], name: "index_authors_books_on_author_id", using: :btree
  add_index "authors_books", ["book_id"], name: "index_authors_books_on_book_id", using: :btree

  create_table "books", force: true do |t|
    t.string   "title"
    t.date     "release_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "author_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "books", ["author_id"], name: "index_books_on_author_id", using: :btree

  create_table "librarians", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.datetime "hire_date"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "librarians", ["email"], name: "index_librarians_on_email", unique: true, using: :btree
  add_index "librarians", ["reset_password_token"], name: "index_librarians_on_reset_password_token", unique: true, using: :btree

  create_table "penalties", force: true do |t|
    t.date     "issue_date"
    t.string   "penalty_type"
    t.integer  "penalty_score"
    t.boolean  "is_settled"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "readers", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.datetime "birth_date"
    t.datetime "register_date"
    t.boolean  "is_blocked"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "readers", ["confirmation_token"], name: "index_readers_on_confirmation_token", unique: true, using: :btree
  add_index "readers", ["email"], name: "index_readers_on_email", unique: true, using: :btree
  add_index "readers", ["reset_password_token"], name: "index_readers_on_reset_password_token", unique: true, using: :btree

  create_table "receipts", force: true do |t|
    t.datetime "issue_date"
    t.integer  "reader_id"
    t.integer  "penalty_id"
    t.integer  "librarian_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "book_id"
    t.integer  "status"
  end

  add_index "receipts", ["book_id"], name: "index_receipts_on_book_id", using: :btree
  add_index "receipts", ["librarian_id"], name: "index_receipts_on_librarian_id", using: :btree
  add_index "receipts", ["penalty_id"], name: "index_receipts_on_penalty_id", using: :btree
  add_index "receipts", ["reader_id"], name: "index_receipts_on_reader_id", using: :btree

end
