# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


=begin
p1 = Part.create(part_number: 1)
p2 = Part.create(part_number: 2)
a1 = Assembly.create(name: "Spajanie Gówna", parts: [p1,p2])
a1.parts.create(part_number: 4)
a1.parts.create(part_number: 3)
=end

require 'random_data'


Book.destroy_all
Reader.destroy_all
AuthorsBooks.destroy_all
Librarian.destroy_all
Receipt.destroy_all

#read1 = Reader.create(name: "#{Random.first_name}", surname: "#{Random.last_name}", birth_date: DateTime.current, register_date: Time.now, is_blocked: "false")
#read2 = Reader.create(name: "#{Random.first_name}", surname: "#{Random.last_name}", birth_date: DateTime.current, register_date: Time.now, is_blocked: "false")

#libr1 = Librarian.create(name: "#{Random.first_name}", surname: "#{Random.last_name}", hire_date: DateTime.current, email: "librarian1@bookstore.ly", )
#libr2 = Librarian.create(name: "#{Random.first_name}", surname: "#{Random.last_name}", hire_date: DateTime.current)

#pen1 = Penalty.create(penalty_score: 15, penalty_type: "fee", issue_date: Time.now, is_settled: "false")
#pen2 = Penalty.create(penalty_score: 25, penalty_type: "fee", issue_date: Time.now, is_settled: "false")
i=1
100.times {
a = Author.create(name: "#{Random.first_name}", surname: "#{Random.last_name}")
b = Book.create(title: "Book#{i}", release_date: Date.current,)
AuthorsBooks.create(book: b, author: a)
i=i+1

#AuthorsBooks.create(author: auth1, book: book1)
#AuthorsBooks.create(author: auth2, book: book2)
}
#Reader.create(name: "Alex", surname: "Kifon", email: "a@a.pl", password: "asdasdasd")
Librarian.create(name: "Alex", surname: "Kifon", email: "a@a.pl", password: "asdasdasd")
#Receipt.create(reader: read1, librarian: libr1, penalty: pen1, book: book1, issue_date: Time.now)
#Receipt.create(reader: read2, librarian: libr2, penalty: pen2, book: book2, issue_date: Time.now)
#pat.appointments.create(physician: phy)

#Administrator.create!(:email => 'admin@example.com', :password => 'password', :password_confirmation => 'password')