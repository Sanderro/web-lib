require 'spec_helper'
require 'random_data'
describe Reader do

  before(:each) do
    #@category = create(:category)
    #@category = create(:category, :name => "Fantasy")
    @reader = Reader.new(name: "readername1", surname: "readername1", birth_date: DateTime.current, register_date: Time.now, is_blocked: "false")

    #reader = build(name: "readername1", surname: "readername1", birth_date: DateTime.current, register_date: Time.now, is_blocked: "false")
  end

  #after(:all) do

  #end
  context 'validations' do
    let(:reader) { Reader.new}
    subject       {reader}

    it 'reader exist' do
        @reader.should_not == nil
    end

    it 'should have name' do
      @reader.name.should_not == nil
    end

    it 'should have surname' do
        @reader.surname.should_not == nil
    end

    it 'should have birth_date' do
      @reader.birth_date.should_not == nil
    end

    it 'should have register date ' do
      @reader.register_date.should_not == nil
    end

    #it 'is valid, gets saved and increases count' do
      #expect {build(:product).save}.to change {Product.count }.by 1
      #expect {@reader.save}.to change { Reader.count }.by 1
    #end


  end
end
